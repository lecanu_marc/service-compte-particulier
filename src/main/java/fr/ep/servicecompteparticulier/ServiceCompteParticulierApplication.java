package fr.ep.servicecompteparticulier;

import com.fasterxml.jackson.annotation.JsonInclude;
import fr.ep.izigloo.core.IziglooCoreConfiguration;
import fr.ep.izigloo.core.logging.IziglooLoggingContext;
import fr.ep.izigloo.monitoring.IziglooMonitoringConfiguration;
import fr.ep.izigloo.security.configuration.IziglooSecurityConfiguration;
import fr.ep.izigloo.servicediscovery.IziglooModuleManagerConfiguration;
import fr.ep.izigloo.swagger.configuration.IziglooSwagger2Configuration;
import fr.ep.izigloo.swagger.properties.IziglooSwaggerContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;

@SpringBootApplication
@Import({
		IziglooCoreConfiguration.class,
		IziglooSwagger2Configuration.class,
		IziglooMonitoringConfiguration.class,
		IziglooSecurityConfiguration.class,
		IziglooModuleManagerConfiguration.class
})
@EnableJpaRepositories(basePackages = {
		"fr.ep.servicecompteparticulier.particulier.repositories",
		"fr.ep.servicecompteparticulier.sociallogin.repositories"
})
public class ServiceCompteParticulierApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceCompteParticulierApplication.class, args);
	}

	@Bean
	public IziglooSwaggerContext iziglooSwaggerContext() {
		return new IziglooSwaggerContext("Service Compte Particulier", "", "v1");
	}
	@Bean
	public IziglooLoggingContext iziglooLoggingContext() {
		return new IziglooLoggingContext("service-compte-particulier");
	}

	@Bean
	public Jackson2ObjectMapperFactoryBean jackson2ObjectMapperFactoryBean() {
		Jackson2ObjectMapperFactoryBean factory = new Jackson2ObjectMapperFactoryBean();
		factory.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		return factory;
	}
}
