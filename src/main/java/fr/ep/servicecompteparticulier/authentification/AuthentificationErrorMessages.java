package fr.ep.servicecompteparticulier.authentification;


import fr.ep.izigloo.core.exception.IziglooServiceError;

import static javax.servlet.http.HttpServletResponse.*;


public class AuthentificationErrorMessages {

    public static final IziglooServiceError SYNTAXE_JSON_INCORRECTE = IziglooServiceError.builder()
            .httpCode(SC_BAD_REQUEST)
            .iziglooCode("01")
            .message("Le json d'entrée est incorrecte. Raison : {0}")
            .buildTechnical();

    public static final IziglooServiceError COMPTE_INEXISTANT = IziglooServiceError.builder()
            .httpCode(SC_PRECONDITION_FAILED)
            .iziglooCode("20-1101")
            .message("Le compte [{0}] n'a pas été trouvé")
            .build();

    public static final IziglooServiceError CONNEXION_IMPOSSIBLE = IziglooServiceError.builder()
            .httpCode(SC_PRECONDITION_FAILED)
            .iziglooCode("20-1102")
            .message("Connexion via le social login impossible")
            .build();

    public static final IziglooServiceError ERR_TOKEN = IziglooServiceError.builder()
            .httpCode(SC_PRECONDITION_FAILED)
            .iziglooCode("20-1103")
            .message("Erreur lors de la récupération du token pour le compte {0}")
            .build();

}