package fr.ep.servicecompteparticulier.authentification.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "google.client")
public class GoogleProperties {

    private String id;
    private String secret;
    private String tokenApiUrl;
    private String utilisateurApiUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getTokenApiUrl() {
        return tokenApiUrl;
    }

    public void setTokenApiUrl(String tokenApiUrl) {
        this.tokenApiUrl = tokenApiUrl;
    }

    public String getUtilisateurApiUrl() {
        return utilisateurApiUrl;
    }

    public void setUtilisateurApiUrl(String utilisateurApiUrl) {
        this.utilisateurApiUrl = utilisateurApiUrl;
    }
}
