package fr.ep.servicecompteparticulier.authentification.configuration.technical;

import fr.ep.izigloo.security.services.RSAKeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;

@Configuration
public class JwtPrivateKeyConfiguration {

    @Autowired
    private RSAKeyService rsaKeyService;

    @Value("${sdsecurity.keypair.path}")
    private String path = null;
    @Value("${sdsecurity.keypair.privateKey}")
    private String privateKey = null;
    @Value("${sdsecurity.keypair.algorithm}")
    private String algorithm = null;

    public String getPath() {
        return path;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    @Bean(name="JwtPrivateKey")
    public PrivateKey privateKey() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        return rsaKeyService.loadPrivateKey(path,privateKey,algorithm);
    }

}
