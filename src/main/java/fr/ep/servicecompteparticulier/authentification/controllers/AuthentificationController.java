package fr.ep.servicecompteparticulier.authentification.controllers;

import fr.ep.izigloo.core.exception.CatchRestThrowable;
import fr.ep.izigloo.core.exception.IziglooException;
import fr.ep.izigloo.core.logging.AutowiredLogging;
import fr.ep.izigloo.core.logging.Logging;
import fr.ep.servicecompteparticulier.authentification.AuthentificationErrorMessages;
import fr.ep.servicecompteparticulier.authentification.model.dto.*;
import fr.ep.servicecompteparticulier.authentification.services.AuthentificationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@Api(value = "Authentification")
@RequestMapping(value = "/v1")
public class AuthentificationController {

    @AutowiredLogging
    private Logging logger = null;

    private final AuthentificationService authentificationService;

    @Autowired
    public AuthentificationController(AuthentificationService authentificationService) {
        this.authentificationService = authentificationService;
    }

    @ApiOperation("Authentification avec France connect")
    @PostMapping("/authentification/france_connect")
    public Token authentificationFranceConnect(@ApiParam(value = "authentification", name = "authentification") @RequestBody Authentification authentification) throws IOException {
        return authentificationService.authentificationFranceConnect(authentification);
    }

    @ApiOperation("Authentification avec google")
    @PostMapping("/authentification/google")
    public Token authentificationGoogle(@ApiParam(value = "authentification", name = "authentification") @RequestBody Authentification authentification) throws IOException {
        return authentificationService.authentificationGoogle(authentification);
    }

    @CatchRestThrowable
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public void handleJsonMappingException(HttpMessageNotReadableException ex) {
        throw new IziglooException(AuthentificationErrorMessages.SYNTAXE_JSON_INCORRECTE, ex.getRootCause().getMessage());
    }
}
