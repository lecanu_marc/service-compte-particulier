package fr.ep.servicecompteparticulier.authentification.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Authentification {

    private final String clientId;
    private final String code;
    private final String redirectUri;
    private final String state;

    @JsonCreator
    public Authentification(@JsonProperty("clientId") String clientId,
                            @JsonProperty("code") String code,
                            @JsonProperty("redirectUri") String redirectUri,
                            @JsonProperty("state") String state) {
        this.clientId = clientId;
        this.code = code;
        this.redirectUri = redirectUri;
        this.state = state;
    }

    public String getClientId() {
        return clientId;
    }

    public String getCode() {
        return code;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public String getState() {
        return state;
    }
}
