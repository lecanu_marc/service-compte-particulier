package fr.ep.servicecompteparticulier.authentification.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableToken.class, include = JsonSerialize.Inclusion.NON_NULL)
@JsonDeserialize(as = ImmutableToken.class)
public interface Token {

    String getToken();
}
