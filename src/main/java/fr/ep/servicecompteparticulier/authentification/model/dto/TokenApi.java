package fr.ep.servicecompteparticulier.authentification.model.dto;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenApi {

    private final String accessToken;
    private final String tokenType;
    private final Integer expiresIn;
    private final String idToken;

    @JsonCreator
    public TokenApi(@JsonProperty("access_token") String accessToken,
                    @JsonProperty("token_type") String tokenType,
                    @JsonProperty("expires_in") Integer expiresIn,
                    @JsonProperty("id_token") String idToken) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
        this.expiresIn = expiresIn;
        this.idToken = idToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public String getIdToken() {
        return idToken;
    }
}
