package fr.ep.servicecompteparticulier.authentification.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UtilisateurApi {

    private String email;
    private String nom;
    private String prenom;

    @JsonCreator
    public UtilisateurApi(@JsonProperty("email") String email,
                          @JsonProperty("family_name") String nom,
                          @JsonProperty("given_name") String prenom) {
        this.email = email;
        this.nom = nom;
        this.prenom = prenom;
    }


    public String getEmail() {
        return email;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }
}
