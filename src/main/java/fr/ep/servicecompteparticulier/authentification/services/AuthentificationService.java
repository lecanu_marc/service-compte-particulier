package fr.ep.servicecompteparticulier.authentification.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ep.izigloo.core.exception.IziglooException;
import fr.ep.izigloo.core.logging.InOutLogger;
import fr.ep.izigloo.security.model.JWTUser;
import fr.ep.izigloo.security.services.JWTTokenService;
import fr.ep.servicecompteparticulier.authentification.AuthentificationErrorMessages;
import fr.ep.servicecompteparticulier.authentification.configuration.properties.FranceConnectProperties;
import fr.ep.servicecompteparticulier.authentification.configuration.properties.GoogleProperties;
import fr.ep.servicecompteparticulier.authentification.configuration.properties.TokenProperties;
import fr.ep.servicecompteparticulier.authentification.model.dto.*;
import fr.ep.servicecompteparticulier.sociallogin.model.dto.CreationSocialLogin;
import fr.ep.servicecompteparticulier.sociallogin.model.dto.ReseauSocial;
import fr.ep.servicecompteparticulier.sociallogin.model.dto.SocialLogin;
import fr.ep.servicecompteparticulier.sociallogin.model.entities.SocialLoginEntity;
import fr.ep.servicecompteparticulier.sociallogin.repositories.SocialLoginRepository;
import fr.ep.servicecompteparticulier.sociallogin.services.SocialLoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.lang.invoke.SwitchPoint;
import java.util.HashSet;
import java.util.Set;

/**
 * Ce service, génère un token avec une date d'expiration définis en configuration et signe le token avec une cle privé
 * dont le path est fournis en configuration.
 */
@Service
public class AuthentificationService {

    private static final Logger logger = LoggerFactory.getLogger(AuthentificationService.class);

    public static final String CLIENT_ID_KEY = "client_id", REDIRECT_URI_KEY = "redirect_uri",
            CLIENT_SECRET = "client_secret", CODE_KEY = "code", GRANT_TYPE_KEY = "grant_type",
            AUTH_CODE = "authorization_code";

    private final JWTTokenService jwtTokenService;
    private final SocialLoginRepository socialLoginRepository;
    private final TokenProperties tokenProperties;
    private final SocialLoginService socialLoginService;
    private final FranceConnectProperties franceConnectProperties;
    private final GoogleProperties googleProperties;
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    @Autowired
    public AuthentificationService(JWTTokenService jwtTokenService,
                                   SocialLoginRepository socialLoginRepository,
                                   TokenProperties tokenProperties,
                                   SocialLoginService socialLoginService,
                                   FranceConnectProperties franceConnectProperties,
                                   GoogleProperties googleProperties) {
        this.jwtTokenService = jwtTokenService;
        this.socialLoginRepository = socialLoginRepository;
        this.tokenProperties = tokenProperties;
        this.socialLoginService = socialLoginService;
        this.franceConnectProperties = franceConnectProperties;
        this.googleProperties = googleProperties;
        this.restTemplate = new RestTemplate();
        this.objectMapper = new ObjectMapper();
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     *
     * @param socialLogin
     * @return
     * @throws IOException
     */
    @InOutLogger
    public Token recuperationTokenReseauSocial(SocialLogin socialLogin) throws IOException {
        JWTUser jwtUser = getJwtReseauSocial(socialLogin);
        String token = jwtTokenService.generateToken(jwtUser, tokenProperties.getDuration());
        return ImmutableToken.builder().token(token).build();
    }

    @InOutLogger
    private JWTUser getJwtReseauSocial(SocialLogin socialLogin) {
        String email = socialLogin.getEmail().toLowerCase();
        Set<String> allowedScopes = new HashSet<String>();

        SocialLoginEntity socialLoginEntity = socialLoginRepository.getByEmailAndNomAndPrenomAndReseauSocial(socialLogin.getEmail(),
                socialLogin.getNom(),
                socialLogin.getPrenom(),
                socialLogin.getReseauSocial());
        if(socialLoginEntity == null){
            throw new IziglooException(AuthentificationErrorMessages.COMPTE_INEXISTANT, email);
        }

        allowedScopes.add("public");
        allowedScopes.add("social_login");
        Long idUtilisateur = socialLoginEntity.getId();
        return (new JWTUser(idUtilisateur, email, allowedScopes));
    }

    public Token authentificationGoogle(Authentification authentification) {
        String tokenGoogle = recupererTokenApi(authentification,ReseauSocial.GOOGLE);
        UtilisateurApi recupererUtilisateurGoogle = recupererUtilisateurApi(tokenGoogle,ReseauSocial.GOOGLE);
        return recupererUtilisateurSocialLogin(recupererUtilisateurGoogle, ReseauSocial.GOOGLE);
    }

    public Token authentificationFranceConnect(Authentification authentification) {
        String tokenFranceConnect = recupererTokenApi(authentification,ReseauSocial.FRANCE_CONNECT);
        UtilisateurApi recupererUtilisateurFranceConnect = recupererUtilisateurApi(tokenFranceConnect,ReseauSocial.FRANCE_CONNECT);
        return recupererUtilisateurSocialLogin(recupererUtilisateurFranceConnect, ReseauSocial.FRANCE_CONNECT);
    }

    private String recupererTokenApi(Authentification authentification, ReseauSocial reseauSocial){
        String tokenApiUrl = null;
        MultiValueMap<String, String> accessData = new LinkedMultiValueMap<>();

        switch (reseauSocial){
            case GOOGLE:
                tokenApiUrl = googleProperties.getTokenApiUrl();
                accessData.add(CLIENT_ID_KEY, googleProperties.getId());
                accessData.add(CLIENT_SECRET, googleProperties.getSecret());
                break;
            case FRANCE_CONNECT:
                tokenApiUrl = franceConnectProperties.getTokenApiUrl();
                accessData.add(CLIENT_ID_KEY, franceConnectProperties.getId());
                accessData.add(CLIENT_SECRET, franceConnectProperties.getSecret());
                break;
        }

        accessData.add(REDIRECT_URI_KEY, authentification.getRedirectUri());
        accessData.add(CODE_KEY, authentification.getCode());
        accessData.add(GRANT_TYPE_KEY, AUTH_CODE);

        ResponseEntity<TokenApi> response = restTemplate.postForEntity(tokenApiUrl, accessData, TokenApi.class);
        HttpStatus status = response.getStatusCode();
        TokenApi restCall = response.getBody();
        return restCall.getAccessToken();
    }

    private UtilisateurApi recupererUtilisateurApi(String token, ReseauSocial reseauSocial){
        String utilisateurApiUrl = null;
        switch (reseauSocial){
            case GOOGLE:
                utilisateurApiUrl = googleProperties.getUtilisateurApiUrl();
                break;
            case FRANCE_CONNECT:
                utilisateurApiUrl = franceConnectProperties.getUtilisateurApiUrl();
                break;
        }
        HttpEntity<String> entity = new HttpEntity<>("", getHeadersWithBearer(token));

        UtilisateurApi utilisateurApi = restTemplate.exchange(utilisateurApiUrl,
                HttpMethod.GET,
                entity,
                UtilisateurApi.class).getBody();

        return utilisateurApi;
    }

    private Token recupererUtilisateurSocialLogin(UtilisateurApi utilisateurApi, ReseauSocial reseauSocial){
        SocialLogin socialLogin;
        if(utilisateurApi != null){
            SocialLoginEntity socialLoginEntity = socialLoginRepository.getByEmailAndNomAndPrenomAndReseauSocial(utilisateurApi.getEmail(),
                    utilisateurApi.getNom(),
                    utilisateurApi.getPrenom(),
                    reseauSocial);
            if(socialLoginEntity == null){
                CreationSocialLogin creationSocialLogin  = objectMapper.convertValue(utilisateurApi, CreationSocialLogin.class);
                creationSocialLogin.setReseauSocial(reseauSocial);
                socialLoginService.creerSocialLogin(creationSocialLogin);
                socialLogin = objectMapper.convertValue(creationSocialLogin, SocialLogin.class);
            }else{
                socialLogin = objectMapper.convertValue(socialLoginEntity, SocialLogin.class);
            }
            try {
                return recuperationTokenReseauSocial(socialLogin);
            }catch(IOException e){
                throw new IziglooException(AuthentificationErrorMessages.ERR_TOKEN, socialLogin.getEmail());
            }
        }

        throw new IziglooException(AuthentificationErrorMessages.CONNEXION_IMPOSSIBLE);
    }


    private MultiValueMap<String, String> getHeadersWithBearer(String token) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Authorization", "Bearer " +token);
        return headers;
    }
}
