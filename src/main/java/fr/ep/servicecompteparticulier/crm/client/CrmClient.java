package fr.ep.servicecompteparticulier.crm.client;

import fr.ep.servicecompteparticulier.particulier.model.entities.ParticulierEntity;
import fr.ep.servicecompteparticulier.sociallogin.model.entities.SocialLoginEntity;

public interface CrmClient {

    /**
     * Créé un particulier dans le CRM
     * @param particulierEntity particulier à créé
     * @return L'identifiant technique du particulier créé dans le CRM
     */
    String creerParticulier(ParticulierEntity particulierEntity);

    /**
     * Créé un particulier dans le CRM
     * @param socialLoginEntity particulier à créé
     * @return L'identifiant technique du particulier créé dans le CRM
     */
    String creerParticulier(SocialLoginEntity socialLoginEntity);
}
