package fr.ep.servicecompteparticulier.crm.client;

import fr.ep.izigloo.core.exception.IziglooServiceError;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

public class CrmClientErrorMessages {

    public static final IziglooServiceError SALESFORCE_API = IziglooServiceError.builder()
            .iziglooCode("02")
            .httpCode(SC_INTERNAL_SERVER_ERROR)
            .message("Une erreur est survenue lors de l'appel à l'API Salesforce. Réponse du serveur : {0}")
            .buildTechnical();
}
