package fr.ep.servicecompteparticulier.crm.client.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "crm.id")
public class CrmIdProperties {

    private RecordType recordType;

    public RecordType getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordType recordType) {
        this.recordType = recordType;
    }

    public static class RecordType {
        private String lead;
        private String personaccount;
        private String maison;
        private String appartement;
        private String bureau;
        private String commerce;
        private String dependance;

        public String getLead() {
            return lead;
        }

        public void setLead(String lead) {
            this.lead = lead;
        }

        public String getPersonaccount() {
            return personaccount;
        }

        public void setPersonaccount(String personaccount) {
            this.personaccount = personaccount;
        }

        public String getMaison() {
            return maison;
        }

        public void setMaison(String maison) {
            this.maison = maison;
        }

        public String getAppartement() {
            return appartement;
        }

        public void setAppartement(String appartement) {
            this.appartement = appartement;
        }

        public String getBureau() {
            return bureau;
        }

        public void setBureau(String bureau) {
            this.bureau = bureau;
        }

        public String getCommerce() {
            return commerce;
        }

        public void setCommerce(String commerce) {
            this.commerce = commerce;
        }

        public String getDependance() {
            return dependance;
        }

        public void setDependance(String dependance) {
            this.dependance = dependance;
        }
    }
}
