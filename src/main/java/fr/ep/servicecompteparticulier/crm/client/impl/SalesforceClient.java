package fr.ep.servicecompteparticulier.crm.client.impl;

import fr.ep.izigloo.salesforcerestclient.SalesforceRestClient;
import fr.ep.izigloo.salesforcerestclient.config.TokenConfiguration;
import fr.ep.izigloo.salesforcerestclient.model.SingleRecordResponse;
import fr.ep.servicecompteparticulier.crm.client.CrmClient;
import fr.ep.servicecompteparticulier.crm.client.configuration.CrmClientApiProperties;
import fr.ep.servicecompteparticulier.crm.client.configuration.CrmIdProperties;
import fr.ep.servicecompteparticulier.crm.client.model.PersonAccount;
import fr.ep.servicecompteparticulier.particulier.model.entities.ParticulierEntity;
import fr.ep.servicecompteparticulier.sociallogin.model.entities.SocialLoginEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesforceClient implements CrmClient {

    private static final Logger LOG = LoggerFactory.getLogger(SalesforceClient.class);
    private static final String PAYS_FRANCE = "France";
    private final SalesforceRestClient salesforceRestClient;
    private final CrmIdProperties crmIdProperties;

    @Autowired
    public SalesforceClient(CrmClientApiProperties apiProperties , CrmIdProperties crmIdProperties) {
        this.salesforceRestClient = new SalesforceRestClient(apiProperties.getHost(), getTokenConfiguration(apiProperties));
        this.crmIdProperties = crmIdProperties;
    }

    private TokenConfiguration getTokenConfiguration(CrmClientApiProperties apiProperties) {
        TokenConfiguration tokenConfiguration = new TokenConfiguration();
        CrmClientApiProperties.Token token = apiProperties.getToken();
        tokenConfiguration.setUsername(token.getUsername());
        tokenConfiguration.setPassword(token.getPassword());
        tokenConfiguration.setGrantType(token.getGrantType());
        tokenConfiguration.setClientId(token.getClientId());
        tokenConfiguration.setClientSecret(token.getClientSecret());
        tokenConfiguration.setDuration(token.getDuration());
        return tokenConfiguration;
    }

    @Override
    public String creerParticulier(ParticulierEntity particulierEntity) {
        if(particulierEntity!=null){
            PersonAccount personAccount = createPersonAccount(particulierEntity);
            SingleRecordResponse response = salesforceRestClient.insertSingleRecord(personAccount);
            return response.getId();
        }
        return null;
    }

    @Override
    public String creerParticulier(SocialLoginEntity socialLoginEntity) {
        if(socialLoginEntity!=null){
            PersonAccount personAccount = createPersonAccount(socialLoginEntity);
            SingleRecordResponse response = salesforceRestClient.insertSingleRecord(personAccount);
            return response.getId();
        }
        return null;
    }

    private PersonAccount createPersonAccount(ParticulierEntity particulierEntity){
        PersonAccount personAccount = new PersonAccount();
        personAccount.setLastName(particulierEntity.getNom());
        personAccount.setFirstName(particulierEntity.getPrenom());
        personAccount.setEmail(particulierEntity.getEmail());
        personAccount.setPersonMailingCountry(PAYS_FRANCE);
        personAccount.setRecordTypeId(crmIdProperties.getRecordType().getPersonaccount());
        return personAccount;
    }

    private PersonAccount createPersonAccount(SocialLoginEntity socialLoginEntity){
        PersonAccount personAccount = new PersonAccount();
        personAccount.setLastName(socialLoginEntity.getNom());
        personAccount.setFirstName(socialLoginEntity.getPrenom());
        personAccount.setEmail(socialLoginEntity.getEmail());
        personAccount.setPersonMailingCountry(PAYS_FRANCE);
        personAccount.setRecordTypeId(crmIdProperties.getRecordType().getPersonaccount());
        return personAccount;
    }
}
