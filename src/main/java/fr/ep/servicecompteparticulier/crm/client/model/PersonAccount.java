package fr.ep.servicecompteparticulier.crm.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ep.izigloo.salesforcerestclient.model.Record;

public class PersonAccount extends Record {

    public static final String OBJECT_NAME = "Account";

    @JsonProperty("firstname")
    private String firstName;
    @JsonProperty("lastname")
    private String lastName;
    @JsonProperty("salutation")
    private String salutation;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Statut__c")
    private String statut;
    @JsonProperty("personemail")
    private String email;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("personMobilePhone")
    private String personMobilePhone;
    @JsonProperty("personMailingCity")
    private String personMailingCity;
    @JsonProperty("personMailingPostalCode")
    private String personMailingPostalCode;
    @JsonProperty("personMailingStreet")
    private String personMailingStreet;
    @JsonProperty("personMailingCountry")
    private String personMailingCountry;
    @JsonProperty("type_Habitat__c")
    private String typeHabitat;
    @JsonProperty("adherent_MGEN__c")
    private Boolean adherentMgen;
    @JsonProperty("RecordTypeId")
    private String recordTypeId;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPersonMobilePhone() {
        return personMobilePhone;
    }

    public void setPersonMobilePhone(String personMobilePhone) {
        this.personMobilePhone = personMobilePhone;
    }

    public String getPersonMailingCity() {
        return personMailingCity;
    }

    public void setPersonMailingCity(String personMailingCity) {
        this.personMailingCity = personMailingCity;
    }

    public String getPersonMailingPostalCode() {
        return personMailingPostalCode;
    }

    public void setPersonMailingPostalCode(String personMailingPostalCode) {
        this.personMailingPostalCode = personMailingPostalCode;
    }

    public String getPersonMailingStreet() {
        return personMailingStreet;
    }

    public void setPersonMailingStreet(String personMailingStreet) {
        this.personMailingStreet = personMailingStreet;
    }

    public String getPersonMailingCountry() {
        return personMailingCountry;
    }

    public void setPersonMailingCountry(String personMailingCountry) {
        this.personMailingCountry = personMailingCountry;
    }

    public String getTypeHabitat() {
        return typeHabitat;
    }

    public void setTypeHabitat(String typeHabitat) {
        this.typeHabitat = typeHabitat;
    }

    public Boolean getAdherentMgen() {
        return adherentMgen;
    }

    public void setAdherentMgen(Boolean adherentMgen) {
        this.adherentMgen = adherentMgen;
    }

    public String getRecordTypeId() {
        return recordTypeId;
    }

    public void setRecordTypeId(String recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    @Override
    public String getObjectName() {
        return OBJECT_NAME;
    }
}
