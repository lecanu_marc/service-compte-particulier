package fr.ep.servicecompteparticulier.particulier;

import fr.ep.izigloo.core.exception.IziglooServiceError;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_PRECONDITION_FAILED;

public class ParticulierErrorMessages {

    public static final IziglooServiceError CHAMP_VIDE = IziglooServiceError.builder()
            .httpCode(SC_BAD_REQUEST)
            .iziglooCode("20-301")
            .message("Le champ {0} est obligatoire et n'est pas renseigné")
            .build();

    public static final IziglooServiceError PRO_EXISTE_DEJA_BDD = IziglooServiceError.builder()
            .httpCode(SC_PRECONDITION_FAILED)
            .iziglooCode("20-302")
            .message("Le compte professionnel avec l'email {0} existe déjà en base")
            .build();

    public static final IziglooServiceError PRO_INEXISTANT_BDD = IziglooServiceError.builder()
            .httpCode(SC_PRECONDITION_FAILED)
            .iziglooCode("20-303")
            .message("Le compte professionnel avec l'email {0} n'existe pas en base")
            .build();

    public static final IziglooServiceError PRO_SANS_ENTREPRISE = IziglooServiceError.builder()
            .httpCode(SC_BAD_REQUEST)
            .iziglooCode("20-304")
            .message("Le compte professionnel avec l'email {0} n'est rattaché à aucune entreprise")
            .build();

    public static final IziglooServiceError INVALID_FIELD = IziglooServiceError.builder()
            .httpCode(SC_BAD_REQUEST)
            .iziglooCode("20-305")
            .message("Les formats des champs suivants sont invalides, veuillez vérifier la saisie : [{0}]")
            .build();

    public static final IziglooServiceError DONNEES_INCOMPLETE = IziglooServiceError.builder()
            .httpCode(SC_BAD_REQUEST)
            .iziglooCode("20-306")
            .message("Les données sont incomplètes pour terminer le traitement. {0}")
            .build();

    public static final IziglooServiceError ERR_INVALID_TOKEN = IziglooServiceError.builder()
            .httpCode(SC_PRECONDITION_FAILED)
            .iziglooCode("20-307")
            .message("Le token [{0}] est invalide")
            .build();

    public static final IziglooServiceError ERR_TOKEN_EXPIRED = IziglooServiceError.builder()
            .httpCode(SC_PRECONDITION_FAILED)
            .iziglooCode("20-308")
            .message("Le token [{0}] a expiré")
            .build();
}
