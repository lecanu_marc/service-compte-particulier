package fr.ep.servicecompteparticulier.particulier.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "particulier.password.token")
public class ParticulierTokenProperties {
    private Integer size;

    private Integer duree;

    public Integer getSize() {
        return size;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }
}
