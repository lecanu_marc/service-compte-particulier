package fr.ep.servicecompteparticulier.particulier.controllers;

import fr.ep.izigloo.core.exception.IziglooException;
import fr.ep.iziglooutils.NullChecker;
import fr.ep.iziglooutils.PatternChecker;
import fr.ep.servicecompteparticulier.particulier.ParticulierErrorMessages;
import fr.ep.servicecompteparticulier.particulier.model.dto.Particulier;
import fr.ep.servicecompteparticulier.particulier.model.dto.ParticulierInscription;
import fr.ep.servicecompteparticulier.particulier.services.ParticulierService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static fr.ep.iziglooutils.NullChecker.field;
import static fr.ep.iziglooutils.PatternChecker.emailField;

@RestController
@RequestMapping(value = "/v1")
public class ParticulierController {

    private final ParticulierService particulierService;

    @Autowired
    public ParticulierController(ParticulierService particulierService){
        this.particulierService = particulierService;

    }

    @RequestMapping(value = "/particuliers", method = RequestMethod.POST)
    public Particulier doPostParticulier(@ApiParam(value = "particulier", name = "particulier", required = true) @RequestBody ParticulierInscription particulierInscription) {
        checkFieldsCreationPart(particulierInscription);
        Particulier particulier = particulierService.ajouterParticulier(particulierInscription);
        return particulier;
    }

    private void checkFieldsCreationPart(ParticulierInscription particulier){
        NullChecker.checkFields(
                field(particulier.getEmail(), "email"),
                field(particulier.getNom(), "nom"),
                field(particulier.getPrenom(), "prenom"),
                field(particulier.getPassword(), "password")
        ).ifNullThrowFormated(nullFields -> new IziglooException(ParticulierErrorMessages.CHAMP_VIDE, nullFields));

        PatternChecker.checkFields(
                emailField(particulier.getEmail(), "email")
        ).ifFailedThrowFormated(failedFields -> new IziglooException(ParticulierErrorMessages.INVALID_FIELD, failedFields));
    }
}
