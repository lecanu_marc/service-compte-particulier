package fr.ep.servicecompteparticulier.particulier.repositories;

import fr.ep.servicecompteparticulier.particulier.model.entities.ParticulierEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface ParticulierRepository extends JpaRepository<ParticulierEntity, Long> {

    ParticulierEntity getByEmail(String email);

    @Modifying
    @Transactional(readOnly = false)
    @Query("delete from ParticulierEntity p where p.email = :email")
    Integer deleteByEmail(@Param("email") String email);
}
