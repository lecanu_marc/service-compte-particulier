package fr.ep.servicecompteparticulier.particulier.services;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ep.izigloo.core.exception.IziglooException;
import fr.ep.servicecompteparticulier.crm.client.impl.SalesforceClient;
import fr.ep.servicecompteparticulier.particulier.ParticulierErrorMessages;
import fr.ep.servicecompteparticulier.particulier.configuration.ParticulierTokenProperties;
import fr.ep.servicecompteparticulier.particulier.model.dto.Particulier;
import fr.ep.servicecompteparticulier.particulier.model.dto.ParticulierInscription;
import fr.ep.servicecompteparticulier.particulier.model.entities.ParticulierEntity;
import fr.ep.servicecompteparticulier.particulier.repositories.ParticulierRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class ParticulierService {

    private static final Logger logger = LoggerFactory.getLogger(ParticulierService.class);

    private final ParticulierRepository particulierRepository;
    private final ParticulierTokenProperties particulierTokenProperties;
    private final SalesforceClient salesforceClient;
    private final BCryptPasswordEncoder passwordEncoder;
    private final ObjectMapper objectMapper;

    @Autowired
    public ParticulierService(ParticulierRepository particulierRepository, ParticulierTokenProperties particulierTokenProperties, SalesforceClient salesforceClient) {
        this.particulierRepository = particulierRepository;
        this.particulierTokenProperties = particulierTokenProperties;
        this.salesforceClient = salesforceClient;
        this.passwordEncoder = new BCryptPasswordEncoder();
        this.objectMapper = new ObjectMapper();
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
    @Transactional(readOnly = false)
    public Particulier ajouterParticulier(ParticulierInscription particulierInscription) {
        ParticulierEntity particulierEntity = objectMapper.convertValue(particulierInscription, ParticulierEntity.class);
        normalizeParticulier(particulierEntity);
        if (particulierRepository.getByEmail(particulierInscription.getEmail()) != null) {
            logger.error("L'adresse mail " + particulierInscription.getEmail() + " existe déjà");
            throw new IziglooException(ParticulierErrorMessages.CHAMP_VIDE,"email");
        }

        // Ajout des valeurs par défaut
        particulierEntity.setDateCreation(new Date());
        particulierEntity.setDateModification(new Date());
        particulierEntity.setPassword(passwordEncoder.encode(particulierInscription.getPassword()));


        //TODO: revoir création d'un compte dans salesforces
        String idCrm = salesforceClient.creerParticulier(particulierEntity);
        particulierEntity.setIdAccountSF(idCrm);
        ParticulierEntity saved = particulierRepository.save(particulierEntity);

        return objectMapper.convertValue(saved,Particulier.class);
    }

    private void normalizeParticulier (ParticulierEntity particulierEntity) {
        particulierEntity.setNom(WordUtils.capitalizeFully(particulierEntity.getNom(), ' '));
        particulierEntity.setPrenom(WordUtils.capitalizeFully(particulierEntity.getPrenom(), ' ', '-'));
    }


    private String buildRandomToken() {
        logger.debug("Génération d'un token");
        return RandomStringUtils.randomAlphanumeric(particulierTokenProperties.getSize());
    }


}
