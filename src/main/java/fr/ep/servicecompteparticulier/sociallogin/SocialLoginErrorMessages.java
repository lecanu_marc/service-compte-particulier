package fr.ep.servicecompteparticulier.sociallogin;

import fr.ep.izigloo.core.exception.IziglooServiceError;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_PRECONDITION_FAILED;

public class SocialLoginErrorMessages {

    public static final IziglooServiceError CHAMP_VIDE = IziglooServiceError.builder()
            .httpCode(SC_BAD_REQUEST)
            .iziglooCode("20-1301")
            .message("Le champ {0} est obligatoire et n'est pas renseigné")
            .build();

    public static final IziglooServiceError EXISTE_DEJA_BDD = IziglooServiceError.builder()
            .httpCode(SC_PRECONDITION_FAILED)
            .iziglooCode("20-1302")
            .message("Le compte avec l'email {0} pour le reseau social {1} existe déjà en base")
            .build();

    public static final IziglooServiceError INEXISTANT_BDD = IziglooServiceError.builder()
            .httpCode(SC_PRECONDITION_FAILED)
            .iziglooCode("20-1303")
            .message("Le compte avec l'email {0} n'existe pas en base")
            .build();

    public static final IziglooServiceError INVALID_FIELD = IziglooServiceError.builder()
            .httpCode(SC_BAD_REQUEST)
            .iziglooCode("20-1305")
            .message("Les formats des champs suivants sont invalides, veuillez vérifier la saisie : [{0}]")
            .build();

    public static final IziglooServiceError DONNEES_INCOMPLETE = IziglooServiceError.builder()
            .httpCode(SC_BAD_REQUEST)
            .iziglooCode("20-1306")
            .message("Les données sont incomplètes pour terminer le traitement. {0}")
            .build();

    public static final IziglooServiceError ERR_INVALID_TOKEN = IziglooServiceError.builder()
            .httpCode(SC_PRECONDITION_FAILED)
            .iziglooCode("20-307")
            .message("Le token [{0}] est invalide")
            .build();

    public static final IziglooServiceError ERR_TOKEN_EXPIRED = IziglooServiceError.builder()
            .httpCode(SC_PRECONDITION_FAILED)
            .iziglooCode("20-308")
            .message("Le token [{0}] a expiré")
            .build();
}
