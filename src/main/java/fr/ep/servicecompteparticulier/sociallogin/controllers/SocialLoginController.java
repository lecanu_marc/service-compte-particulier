package fr.ep.servicecompteparticulier.sociallogin.controllers;

import fr.ep.izigloo.core.exception.IziglooException;
import fr.ep.iziglooutils.NullChecker;
import fr.ep.iziglooutils.PatternChecker;
import fr.ep.servicecompteparticulier.sociallogin.SocialLoginErrorMessages;
import fr.ep.servicecompteparticulier.sociallogin.model.dto.CreationSocialLogin;
import fr.ep.servicecompteparticulier.sociallogin.services.SocialLoginService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static fr.ep.iziglooutils.NullChecker.field;
import static fr.ep.iziglooutils.PatternChecker.emailField;

@RestController
@RequestMapping(value = "/v1")
public class SocialLoginController {

    private final SocialLoginService socialLoginService;

    @Autowired
    public SocialLoginController(SocialLoginService socialLoginService){
        this.socialLoginService = socialLoginService;
    }

    @ApiOperation("Création d'un compte via le social login")
    @PostMapping("/social_login")
    public void creerSocialLogin(@RequestBody CreationSocialLogin creationSocialLogin) {
        checkFieldsCreationSocialLogin(creationSocialLogin);
        socialLoginService.creerSocialLogin(creationSocialLogin);
    }

    private void checkFieldsCreationSocialLogin(CreationSocialLogin socialLogin){
        NullChecker.checkFields(
                field(socialLogin.getEmail(), "email"),
                field(socialLogin.getNom(), "nom"),
                field(socialLogin.getPrenom(), "prenom"),
                field(socialLogin.getReseauSocial(), "reseau social")
        ).ifNullThrowFormated(nullFields -> new IziglooException(SocialLoginErrorMessages.CHAMP_VIDE, nullFields));

        PatternChecker.checkFields(
                emailField(socialLogin.getEmail(), "email")
        ).ifFailedThrowFormated(failedFields -> new IziglooException(SocialLoginErrorMessages.INVALID_FIELD, failedFields));
    }
}
