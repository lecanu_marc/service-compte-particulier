package fr.ep.servicecompteparticulier.sociallogin.model.dto;


public class CreationSocialLogin {

    private String email;
    private String nom;
    private String prenom;
    private ReseauSocial reseauSocial;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public ReseauSocial getReseauSocial() {
        return reseauSocial;
    }

    public void setReseauSocial(ReseauSocial reseauSocial) {
        this.reseauSocial = reseauSocial;
    }
}
