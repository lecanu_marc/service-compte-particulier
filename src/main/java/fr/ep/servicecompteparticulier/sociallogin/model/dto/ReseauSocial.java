package fr.ep.servicecompteparticulier.sociallogin.model.dto;

import org.apache.commons.lang3.StringUtils;

public enum ReseauSocial {
    GOOGLE("Google"),
    FRANCE_CONNECT("France connect");

    private String name;

    ReseauSocial(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public String capitalize() {
        return StringUtils.capitalize(this.name);
    }

    @Override
    public String toString() {
        return name;
    }
}
