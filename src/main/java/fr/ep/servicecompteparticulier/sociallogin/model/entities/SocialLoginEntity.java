package fr.ep.servicecompteparticulier.sociallogin.model.entities;

import fr.ep.servicecompteparticulier.sociallogin.model.dto.ReseauSocial;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "social_login")
public class SocialLoginEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "date_creation")
    private Date dateCreation;

    private String email;
    private String nom;
    private String prenom;

    @Column(name = "id_crm")
    private String idCrm;

    @Column(name = "reseau_social")
    private ReseauSocial reseauSocial;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getIdCrm() {
        return idCrm;
    }

    public void setIdCrm(String idCrm) {
        this.idCrm = idCrm;
    }

    public ReseauSocial getReseauSocial() {
        return reseauSocial;
    }

    public void setReseauSocial(ReseauSocial reseauSocial) {
        this.reseauSocial = reseauSocial;
    }
}
