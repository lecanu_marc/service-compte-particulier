package fr.ep.servicecompteparticulier.sociallogin.repositories;

import fr.ep.servicecompteparticulier.sociallogin.model.dto.ReseauSocial;
import fr.ep.servicecompteparticulier.sociallogin.model.entities.SocialLoginEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface SocialLoginRepository extends JpaRepository<SocialLoginEntity, Long> {

    SocialLoginEntity getByEmailAndReseauSocial(String email, ReseauSocial reseauSocial);
    SocialLoginEntity getByEmailAndNomAndPrenomAndReseauSocial(String email, String nom, String prenom, ReseauSocial reseauSocial);

}
