package fr.ep.servicecompteparticulier.sociallogin.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ep.izigloo.core.exception.IziglooException;
import fr.ep.servicecompteparticulier.authentification.services.AuthentificationService;
import fr.ep.servicecompteparticulier.crm.client.impl.SalesforceClient;
import fr.ep.servicecompteparticulier.sociallogin.SocialLoginErrorMessages;
import fr.ep.servicecompteparticulier.sociallogin.model.dto.CreationSocialLogin;
import fr.ep.servicecompteparticulier.sociallogin.model.dto.SocialLogin;
import fr.ep.servicecompteparticulier.sociallogin.model.entities.SocialLoginEntity;
import fr.ep.servicecompteparticulier.sociallogin.repositories.SocialLoginRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SocialLoginService {

    private static final Logger logger = LoggerFactory.getLogger(SocialLoginService.class);

    private final SocialLoginRepository socialLoginRepository;
    private final SalesforceClient salesforceClient;
    private final ObjectMapper objectMapper;

    @Autowired
    public SocialLoginService(SocialLoginRepository socialLoginRepository, SalesforceClient salesforceClient){
        this.socialLoginRepository = socialLoginRepository;
        this.salesforceClient = salesforceClient;
        this.objectMapper = new ObjectMapper();
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public SocialLogin creerSocialLogin(CreationSocialLogin creationSocialLogin) {
        logger.debug("Création d'un compte social login : nom '{}', prenom '{}', email '{}', reseau social '{}'",creationSocialLogin.getNom(),creationSocialLogin.getPrenom(),creationSocialLogin.getEmail(),creationSocialLogin.getReseauSocial());
        SocialLoginEntity socialLoginEntity = objectMapper.convertValue(creationSocialLogin, SocialLoginEntity.class);
        if(socialLoginRepository.getByEmailAndReseauSocial(creationSocialLogin.getEmail(),creationSocialLogin.getReseauSocial()) != null){
            throw new IziglooException(SocialLoginErrorMessages.EXISTE_DEJA_BDD,creationSocialLogin.getEmail(), creationSocialLogin.getReseauSocial().name());
        }
        socialLoginEntity.setDateCreation(new Date());
        String idCrm = salesforceClient.creerParticulier(socialLoginEntity);
        socialLoginEntity.setIdCrm(idCrm);
        SocialLoginEntity saved = socialLoginRepository.save(socialLoginEntity);
        return objectMapper.convertValue(saved, SocialLogin.class);
    }
}
